<?php
/**
 * The main template file
 *
 * @package WordPress
 * @subpackage AthenaVirtual
 * @since AthenaVirtual 1.0
 */
 ?>
 <!DOCTYPE html>

 <html <?php language_attributes(); ?> class="no-js">
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link rel="shortcut icon" href="/favicon.ico">
    <link href="https://assets.calendly.com/assets/external/widget.css" rel="stylesheet">
    <title>Athena</title>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-KJZRMWB');</script>
    <!-- End Google Tag Manager -->
    <!-- Start of athenavirtual Zendesk Widget script -->
    <script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=2079f837-27a1-4c6b-bbab-e442819426d8"></script>
    <!-- End of athenavirtual Zendesk Widget script -->
    <!-- End of YotPo Widget script -->
    <script type="text/javascript">
    (function e(){var e=document.createElement("script");e.type="text/javascript",e.async=true,e.src="//staticw2.yotpo.com/M2oZvGiZq5VLR62XhQ132qZANzLS7mbPKgS20QIJ/widget.js";var t=document.getElementsByTagName("script")[0];t.parentNode.insertBefore(e,t)})();
    </script>
    <!-- End of YotPo Widget script -->

    <script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js">
    </script>
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KJZRMWB"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div id="main">
      <?php wp_footer(); ?>
    </div>
  </body>
</html>