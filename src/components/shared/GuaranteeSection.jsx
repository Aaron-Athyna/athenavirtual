import React from 'react'
import Section from '../shared/Section';

export default ({ backgroundColor }) => (
  <Section className="guaranteeSection" backgroundColor={backgroundColor}>
    <div className="guaranteeCard">
      <h5>Our Mafia Offer</h5>
      <h2 className="bold">try us for 90 days</h2>
      <p>What sets us apart is our #HardWorkIsForLosers System, which we’ve developed to ensure you and your VA have a successful relationship. We’re so confident in our system that if you are not happy after 90 days, we’ll give you your money back x 200 percent!</p>
      <h4>Yeah that’s right mate, double.</h4>
    </div>
  </Section>
);