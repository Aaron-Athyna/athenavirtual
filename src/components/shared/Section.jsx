import React from 'react';

export default ({ children, className, backgroundColor, color, backgroundImage, style }) => (
  <div
    className={`section ${className || ''}`}
    style={{
      backgroundColor,
      color,
      backgroundImage: backgroundImage && `url(${backgroundImage})`
    }}>
    <div className="sectionInner">
      {children}
    </div>
  </div>
)