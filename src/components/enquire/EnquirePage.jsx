import React from 'react'
import Section from '../shared/Section';

export default class EnquirePage extends React.Component {
  render() {
    return (
      <Section backgroundColor="#DAE3E7" className="enquireFormSection align-center">
        <h3>Congrats on making the decision to give a life and get a life, mate.</h3>
        <p style={{ maxWidth: '40em', margin: '0 auto' }}>
          Simply fill in your details below and we'll give you a call to talk through your business needs.<br/>
          We'll then send you a list of candidates from our team of vetted and educated workers who you will be able to interview via Zoom. From three you can choose who you vibed with most.<br/>
          <br/>
          Alright, that's enough banter from us. Let's get the ball rolling.
        </p>

        <form
          method="POST"
          action="https://athenavirtual.activehosted.com/proc.php"
          className="enquireForm"
          id="_form_1_">
          <input type="hidden" name="u" value="1" />
          <input type="hidden" name="f" value="1" />
          <input type="hidden" name="s" />
          <input type="hidden" name="c" value="0" />
          <input type="hidden" name="m" value="0" />
          <input type="hidden" name="act" value="sub" />
          <input type="hidden" name="v" value="2" />
          <div className="row">
            <input
              type="text"
              className="field"
              name="firstname"
              placeholder="First Name*"
              required
            />
            <input
              type="text"
              className="field"
              name="lastname"
              placeholder="Last Name*"
              required
            />
          </div>
          <div className="row">
            <input
              type="text"
              className="field"
              name="email"
              placeholder="Email*"
              required
            />
          </div>
          <div className="row">
            <input
              type="text"
              className="field"
              name="phone"
              placeholder="Phone*"
              required
            />
          </div>
          <div className="row">
            <select className="field" name="field[2]" required>
              <option selected>
                Preferred Package...
              </option>
              <option value="Entry">
                Entry
              </option>
              <option value="Basic">
                Basic
              </option>
              <option value="Premium">
                Premium
              </option>
            </select>
            <input
              type="text"
              className="field"
              name="field[3]"
              placeholder="Industry Type*"
              required
            />
          </div>
          <button id="_form_1_submit" type="submit" className="button">
            Submit
          </button>
        </form>
      </Section>
    );
  }
}