import React from 'react';
import { Link } from 'react-router-dom';

import HeroSection from '../shared/HeroSection';
import FourStepSection from './FourStepSection';
import VideoSection from './VideoSection';
import GuaranteeSection from '../shared/GuaranteeSection';
import TestimonialSection from './TestimonialSection';

export default () => (
  <div>
    <HeroSection
      backgroundImage={`${AssetsPath}/hero2.jpg`}>
      <div>
        <h1>welcome to athena virtual,<br/>the new way to hire a virtual assistant.</h1>
        <Link to="/process" className="button inline" style={{ marginTop: '1em' }}>
          Hire Your VA
        </Link>
      </div>
    </HeroSection>
    <FourStepSection />
    <VideoSection />
    <GuaranteeSection
      backgroundColor="#F5F5F4"
    />
    <TestimonialSection />
  </div>
)