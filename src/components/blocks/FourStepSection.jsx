import React from 'react';
import Section from '../shared/Section';
import { linebreaks } from '../../lib/StringUtils';

export default (props) => {
  const {
    heading,
    backgroundColor,
    textColor,
    callToAction,
    callToActionUrl
  } = props;

  const renderStep = step => (
    <div className="step">
      <div className="stepIcon" style={{ backgroundImage: `url(${props[`step${step}Icon`]})` }}/>
      <div className="flex-auto">
        <h4>{props[`step${step}Title`]}</h4>
        <p>{linebreaks(props[`step${step}Content`])}</p>
      </div>
    </div>
  );

  return (
    <Section
      color={textColor}
      backgroundColor={backgroundColor}>
      <div className="fourStepSection">
        <h3>{linebreaks(heading)}</h3>

        <div className="flex">
          {renderStep(1)}
          {renderStep(2)}
        </div>

        <div className="flex">
          {renderStep(3)}
          {renderStep(4)}
        </div>

        <div className="cta">
          <a className="button" href={callToActionUrl}>{callToAction}</a>
        </div>
      </div>
    </Section>
  );
}