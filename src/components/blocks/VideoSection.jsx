import React from 'react'
import Section from '../shared/Section';

export default ({ heading, textColor, backgroundColor, videoUrl }) => (
  <Section
    color={textColor}
    backgroundColor={backgroundColor}>
    <div className="videoSection">
      <h3>{heading}</h3>
      <iframe
        width="560"
        height="315"
        src="https://www.youtube.com/embed/nQgbIOBzL_s"
        frameborder="0"
        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
        allowfullscreen
      />
    </div>
  </Section>
)