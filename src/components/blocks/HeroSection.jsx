import React from 'react'
import Section from '../shared/Section';

export default ({ heading, backgroundColor, backgroundImage,  textColor }) => (
  <Section
    color={textColor}
    backgroundColor={backgroundColor}
    backgroundImage={backgroundImage}>
    <div className="heroSection">
      <h3>{heading}</h3>
    </div>
  </Section>
)