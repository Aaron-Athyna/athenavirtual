import React from 'react';
import cx from 'classnames';
import { Link } from 'react-router-dom';
import { Menu, X } from 'react-feather';
import backTop from 'back-top';

export default class Header extends React.Component {
  constructor() {
    super(...arguments);

    this.state = {
      menuOpen: false
    }

    this.handleMenuClick = this.handleMenuClick.bind(this);
  }

    
  componentDidMount() {
    backTop({ className: 'button back-top' });
    this.updateHeaderOffset();
    window.addEventListener('resize', this.updateHeaderOffset.bind(this));
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateHeaderOffset.bind(this));
  }

  updateHeaderOffset() {
    const headerEl = document.getElementsByTagName('header')[0];
    const contentEl = document.getElementById('content');
    contentEl.style.paddingTop = `${headerEl.offsetHeight}px`;
  }

  handleMenuClick() {
    this.setState(({ menuOpen }) => ({ menuOpen: !menuOpen }));
  }

  render() {
    const { menuOpen } = this.state;

    return (
      <header>
        <div className="banner">
          <Link to="/process">Hire Your VA</Link>
        </div>
        <nav className="navigation" role="navigation">
          <div className="logo">
            <Link to="/">
              <img src={`${AthenaVirtual.URL.root}wp-content/themes/athenavirtual/dist/images/athenalogo.svg`} />
            </Link>
          </div>
          <div className="mainNavigation">
            <ul className="navigationList">
              <NavigationItem
                name="Home"
                path="/"
              />
              <NavigationItem
                name="Blog"
                path="/blog"
              />
              <NavigationItem
                name="About"
                path="/about"
              />
              <NavigationItem
                name="Help"
                url="https://athenavirtual.zendesk.com"
              />
              <NavigationItem
                name="Hire Your VA"
                path="/process"
                button
              />
            </ul>
          </div>
          <div className="mobileNavigation">
            <div className="mobileNavigationIcon" onClick={this.handleMenuClick}>
              {menuOpen ? <X/> : <Menu/>}
            </div>
            <ul className={cx('navigationList', { open: menuOpen })}>
              <Link to="/">Home</Link>
              <Link to="/blog">Blog</Link>
              <Link to="/about">About</Link>
              <a href="https://athenavirtual.zendesk.com" target="_blank">Help</a>
              <Link to="/process">Hire Your VA</Link>
            </ul>
          </div>
        </nav>
      </header>
    );
  }
}

function NavigationItem({ path, url, name, button }) {
  return (
    <li
      className={cx('navigationItem', {
        active: window.location.pathname.replace(/\/$/, "") === path,
        underline: !button
      })}>
      {url &&
        <a className={cx({ button })} target="_blank" href={url}>{name}</a>
      }
      {!url && path &&
        <Link className={cx({ button })} to={path}>{name}</Link>
      }
    </li>
  );
}