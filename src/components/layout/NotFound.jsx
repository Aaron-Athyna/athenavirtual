import React from 'react'

export default () => (
  <div className="section">
    <div className="sectionInner flex items-center justify-center" style={{ textAlign: 'center', height: '30vh' }}>
      Sorry, this page could not be found
    </div>
  </div>
);