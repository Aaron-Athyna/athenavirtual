import React from 'react'
import Section from '../shared/Section';

export default () => (
  <Section
    className="yotpoSection"
    backgroundColor="#FFFFFF">
    <h2 className="bold">leave a review</h2>
    <div>
      <div
        className="yotpo yotpo-main-widget"
        data-product-id="AthenaVirtual"
        data-price="N/A"
        data-currency="AUD"
        data-name="Athena Virtual"
        data-url="athenavirtual.com">
      </div>
    </div>
  </Section>
)