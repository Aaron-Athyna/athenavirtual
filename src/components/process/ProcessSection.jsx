import React from 'react'
import Section from '../shared/Section';

export default () => {
  const LeftBlock = ({ subheading, heading, body, image, icon }) => (
    <div className="processBlock left">
      <div className="contentWrapper">
        <h5>{subheading}</h5>
        <h2 className="bold">{heading}</h2>
        <p>{body}</p>
      </div>
      <div
        className="imageWrapper"
        style={{
          backgroundImage: `url(${AssetsPath}/${image || icon})`,
          backgroundSize: icon ? 'auto 60%' : 'contain',
          backgroundPosition: icon ? 'center' : 'right'
        }}
      />
    </div>
  );
 
  const RightBlock = ({ subheading, heading, body, image, icon }) => (
    <div className="processBlock right">
      <div
        className="imageWrapper"
        style={{
          backgroundImage: `url(${AssetsPath}/${image || icon})`,
          backgroundSize: icon ? 'auto 60%' : 'contain',
          backgroundPosition: icon ? 'center' : 'left'
        }}
      />
      <div className="contentWrapper">
        <h5>{subheading}</h5>
        <h2 className="bold">{heading}</h2>
        <p>{body}</p>
      </div>
    </div>
  );

  return (
    <Section
      className="processSection"
      backgroundColor="#D8E3E8">
      <LeftBlock
        subheading="Plan To Win"
        heading="systemize"
        icon="systemize.png"
        body={(
          <span>
            The first step is systemising the processes in your business to set you on a path for success.<br/><br/>
Every business needs systems in order to move forward and thrive. Put the systems in place now so you can excel into the future
          </span>
        )}
      />
      <RightBlock
        subheading="Take Back Your Time"
        heading="automate"
        image="automate.png"
        body={(
          <span>
            Man is a tool using machine - harness the power of our automation process to take back control of the only non renewable resource in your life.<br/><br/>
            Time.
          </span>
        )}
      />
      <LeftBlock
        subheading="Grow Your Business"
        heading="scale"
        icon="scale.png"
        body={(
          <span>
            Now that you’ve built your team and freed up your time you can tackle the big projects in your business.<br/><br/>
            This floods your business with new opportunity and revenues, allowing you to ...
          </span>
        )}
      />
      <RightBlock
        subheading="Own The Game"
        heading="win"
        image="win.png"
        body={(
          <span>
            Sit back knowing that your business growth has helped give back life to an entrepreneur on the other side of the globe.<br/><br/>
            Changing the way we work. The ultimate freedom in the new economy.
          </span>
        )}
      />
    </Section>
  );
}