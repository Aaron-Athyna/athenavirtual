import React from 'react'
import Section from '../shared/Section';
import PricingPlan from './PricingPlan';

export default () => (
  <Section
    className="pricingSection"
    backgroundColor="#FFFFFF">
    <h2 className="bold">what packages can I choose from?</h2>
    <div className="pricingPlans">
      <PricingPlan
        name="entry"
        hours={10}
        price="10.00"
        backgroundColor="#87939B"
        features={[
          'Customer service.',
          'Inbox management.',
          'Data entry.',
          'Calendar management.',
          'Call scheduling.'
        ]}
      />

      <PricingPlan
        name="premium"
        hours={40}
        price="6.50"
        backgroundColor="#3B455C"
        large
        features={[
          'Full marketing automations.',
          'Podcast and video editing.',
          'Content creation.',
          'Community management.',
          'Travel and event co-ordination.'
        ]}
      />

      <PricingPlan
        name="basic"
        hours={20}
        price="8.25"
        backgroundColor="#8097A4"
        features={[
          'Book-keeping and invoicing.',
          'Social media marketing.',
          'Email communications.',
          'Copywriting.',
          'Website management.'
        ]}
      />
    </div>
  </Section>
)