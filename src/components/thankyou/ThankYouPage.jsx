import React from 'react';
import Section from '../shared/Section';

export default class ThankYouPage extends React.Component {

  render() {
    return (
      <Section backgroundColor="#DAE3E7" className="thankyouSection align-center">
        <h2>Thanks mate.</h2>
        <p>One of our team members will be in touch soon.</p>
        <div>
          <br/><br/><br/>
          <h3>Skip The Queue - Book Your Interview Today</h3>
          <br/>
          <div
            className="calendly-inline-widget"
            data-url="https://calendly.com/athenavirtualconsult"
            style={{ minWidth: '320px', height: '580px' }}
          />
        </div>
      </Section>
    );
  }
}