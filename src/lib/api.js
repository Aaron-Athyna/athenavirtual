export default class Api {
  constructor(url) {
    this.url = url;
  }

  getAllPosts() {
    return new Promise((resolve, reject) => {
      const endpoint = `${this.url}/posts`;
      fetch(endpoint)
        .then(response => response.json())
        .then(posts => resolve(posts))
        .catch(error => console.error(error));
    })
  }

  getPost(postId) {
    return new Promise((resolve, reject) => {
      const endpoint = `${this.url}/posts/${postId}`;
      fetch(endpoint)
        .then(response => response.json())
        .then(post => resolve(post))
        .catch(error => console.error(error));
    })
  }

  getAllPages() {
    return new Promise((resolve, reject) => {
      const endpoint = `${this.url}/pages`;
      fetch(endpoint)
        .then(response => response.json())
        .then(pages => resolve(pages))
        .catch(error => console.error(error));
    })
  }

  getPage(pageId) {
    return new Promise((resolve, reject) => {
      const endpoint = `${this.url}/pages/${pageId}`
      fetch(endpoint)
        .then(response => response.json())
        .then(page => resolve(page))
        .catch(error => console.error(error))
    })
  }

  getUser(userId) {
    return new Promise((resolve, reject) => {
      const endpoint = `${this.url}/users/${userId}`
      fetch(endpoint)
        .then(response => response.json())
        .then(user => resolve(user))
        .catch(error => console.error(error))
    })
  }
}