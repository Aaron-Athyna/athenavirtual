import React from 'react';

export function linebreaks(string) {
  return string.split('\n').map((item, key) => { return <span key={key}>{item}<br/></span> })
}

export default { linebreaks };