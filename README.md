# Athena Wordpress Theme

## Install Dependencies

```
npm install
```

## Build

```
npm run build
```

## Watch

```
npm run watch
```

## Run Server

I would recommend using https://mamp.info as the wordpress server, directed to a version of wordpress with this theme installed.

## Deploy

To deploy the site, upload the theme as a .zip to the web host, and extract.